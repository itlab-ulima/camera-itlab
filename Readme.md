# ITLab Camera Screen Composable

## Requirements

- Compose version equal or higher than 1.3.0 (in project's build.gradle)
- Kotlin version equal or higher than 1.7.10 (in project's build.gradle)
- minSDK 22 or higher (in module's build.gradle)

## Dependencies

In module's build.gradle:

```
implementation 'com.gitlab.itlab-ulima:camera-itlab:1.1'
```

## Usage

Before used, you must ask for camera permission:

``` xml
    <uses-feature android:name="android.hardware.camera.any" />
    <uses-permission android:name="android.permission.CAMERA" />
```

``` kotlin
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ITLCameraScreen(
                appName = getString(R.string.app_name),
                onImageAccepted = {
                    finish()
                },
                after = null
            )
        }

        requestCameraPermission()
    }

    private val requestPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted ->
        if (isGranted) {
            Log.i("MainActivity", "Permisos otorgados")
        }else {
            Log.i("MainActivity", "Permission no otorgados")
        }
    }

    private fun requestCameraPermission() {
        when {
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED -> {
                Log.i("MainActivity", "Permisos ya han sido otorgados")
            }

            ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.CAMERA
            ) -> {
                // Se debe de mostrar un dialog con informacion de
                // porque dbe de habilitar los permisos de camera.
                Log.i("MainActivity", "Show camera permissions dialog")
            }

            else -> requestPermissionLauncher.launch(Manifest.permission.CAMERA)
        }
    }
}
```

Yo have to use the ITLCameraScreen composable. If you don't declare an after composable, it loads
the preview screen that asks for aprobation or rejection of the photo. On image rejection the image
is deleted from the device.

## Author

- Hernan Quintana (hernan@jsatch.com)