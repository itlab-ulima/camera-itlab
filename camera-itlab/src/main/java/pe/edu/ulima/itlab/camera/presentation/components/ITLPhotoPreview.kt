package pe.edu.ulima.itlab.camera.presentation.components

import android.net.Uri
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun ITLPhotoPreview(
    photoUri : Uri,
    onImageAccepted : (Uri) -> Unit,
    onRejected : (Uri) -> Unit
) {
    Box(
        modifier = Modifier.fillMaxSize()
    ) {
        GlideImage(
            model = photoUri,
            contentDescription = "Photo image taken",
            modifier = Modifier.fillMaxSize()
                .align(Alignment.TopCenter)
        )

        PreviewControls(
            previewUIAction = { previewUIAction ->
                when (previewUIAction) {
                    is PreviewUIAction.AcceptImage -> {
                        onImageAccepted(photoUri)
                    }

                    is PreviewUIAction.RejectImage -> {
                        onRejected(photoUri)
                    }
                }
            },
            modifier = Modifier.align(Alignment.BottomCenter)
        )
    }
}