package pe.edu.ulima.itlab.camera.presentation.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.sharp.Check
import androidx.compose.material.icons.sharp.Delete
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

@Composable
fun PreviewControls(
    previewUIAction: (PreviewUIAction) -> Unit,
    modifier : Modifier
) {
    Row(
        modifier = modifier
            .fillMaxWidth()
            .background(Color.Black)
            .padding(16.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {

        ButtonControl(
            Icons.Sharp.Check,
            "Accept photo taken",
            modifier = Modifier.size(64.dp),
            onClick = { previewUIAction(PreviewUIAction.AcceptImage) }
        )

        ButtonControl(
            Icons.Sharp.Delete,
            "Discard photo taken",
            modifier = Modifier
                .size(64.dp)
                .padding(1.dp)
                .border(1.dp, Color.White, CircleShape),
            onClick = { previewUIAction(PreviewUIAction.RejectImage) }
        )
    }
}