package pe.edu.ulima.itlab.camera.presentation

import android.net.Uri
import android.util.Log
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import java.io.File

@Composable
fun ITLCameraScreen(
    appName : String,
    onImageAccepted : (Uri) -> Unit = {
        Log.d("ITLCameraScreen", "Image Accepted")
    },
    onRejected : (Uri) -> Unit = {
        val file = File(it.path!!)
        try {
            file.delete()
        }catch (e : Exception) {
            Log.e("ITLCameraScreen", e.message!!)
        }

        Log.d("ITLCameraScreen", "Image deleted on Rejection")
    },
    after: (@Composable (Uri) -> Unit)?
) {
    val photoUri = remember {
        mutableStateOf(Uri.EMPTY)
    }

    if (photoUri.value != Uri.EMPTY) {
        if (after == null) {
            pe.edu.ulima.itlab.camera.presentation.components.ITLPhotoPreview(
                photoUri = photoUri.value,
                onImageAccepted = onImageAccepted,
                onRejected = onRejected
            )
        }else {
            after(photoUri.value)
        }

    }else {
        pe.edu.ulima.itlab.camera.presentation.components.ITLCameraView(
            onImageCaptured = { uri, fromGallery ->
                Log.d("ITLCameraScreen", "Image Uri Captured from Camera View")
                photoUri.value = uri

            },
            onError = {
                Log.e("ITLCameraScreen", "Error using Camera View")
            },
            appName = appName
        )
    }
}