package pe.edu.ulima.itlab.camera.presentation.components

import android.content.Context
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.util.Size
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.platform.LocalContext
import androidx.core.content.ContextCompat
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

@Composable
fun ITLCameraView(
    onImageCaptured: (Uri, Boolean) -> Unit,
    onError: (ImageCaptureException) -> Unit,
    appName : String
) {
    //1. Configuracion
    val lensFacing = remember {
        mutableStateOf(CameraSelector.LENS_FACING_BACK)
    }
    val context = LocalContext.current

    val imageCapture = remember {
        ImageCapture.Builder()
            .setTargetResolution(Size(1080, 1920))
            .build()
    }

    val galleryLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.GetContent()
    ) { uri ->
        if (uri != null) onImageCaptured(uri, true) // Se carga de la galeria
    }

    CameraPreviewView(
        imageCapture = imageCapture,
        lensFacing = lensFacing.value
    ) { cameraUIAction ->
        when (cameraUIAction) {
            is CameraUIAction.OnCameraClick -> {
                takePhoto(
                    filenameFormat = "dd-M-yyyy_hh:mm:ss",
                    imageCapture = imageCapture,
                    outputDirectory = context.getOutputDirectory(appName),
                    executor = Executors.newSingleThreadExecutor(),
                    onImageCaptured = onImageCaptured,
                    onError = onError
                )
            }

            is CameraUIAction.OnSwitchCameraClick -> {
                lensFacing.value =
                    if (lensFacing.value == CameraSelector.LENS_FACING_BACK) CameraSelector.LENS_FACING_FRONT
                    else CameraSelector.LENS_FACING_BACK
            }

            is CameraUIAction.OnGalleryViewClick -> {
                if (true == context.getOutputDirectory(appName).listFiles()?.isNotEmpty()) {
                    galleryLauncher.launch("image/*")
                }
            }
        }
    }
}

private fun takePhoto(
    filenameFormat : String,
    imageCapture : ImageCapture,
    outputDirectory : File,
    executor : Executor,
    onImageCaptured : (Uri, Boolean) -> Unit,
    onError : (ImageCaptureException) -> Unit
) {
    val photoFile = File(
        outputDirectory,
        SimpleDateFormat(filenameFormat, Locale.US).format(System.currentTimeMillis())
    )

    val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile).build()
    
    imageCapture.takePicture(outputOptions, executor, object : ImageCapture.OnImageSavedCallback {
        override fun onImageSaved(outputFileResults: ImageCapture.OutputFileResults) {
            onImageCaptured(outputFileResults.savedUri!!, false)
        }

        override fun onError(exception: ImageCaptureException) {
            onError(exception)
        }
    })
}

private suspend fun Context.getCameraProvider() : ProcessCameraProvider = suspendCoroutine { continuation ->
    ProcessCameraProvider.getInstance(this).also { cameraProvider ->
        cameraProvider.addListener( {
            continuation.resume(cameraProvider.get())
        }, ContextCompat.getMainExecutor(this))
    }
}

private fun Context.getOutputDirectory(appName: String) : File {

    val mediaDir = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        File(MediaStore.Images.Media.RELATIVE_PATH, appName).apply { mkdirs() }
    } else {
        this.externalMediaDirs.firstOrNull()?.let {
            File(it, appName).apply { mkdirs() }
        }
    }

    return if (mediaDir != null && mediaDir.exists())
        mediaDir else this.filesDir
}