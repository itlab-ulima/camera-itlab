package pe.edu.ulima.itlab.camera.presentation.components

sealed class PreviewUIAction {
    object AcceptImage : PreviewUIAction()
    object RejectImage : PreviewUIAction()
}